GNU Multiple Precision (GMP) examples.

Prime numbers, fibonacci sequency and factorization examples all written in good old school C

(C) Nuno Ferreira 2017

May be distributed under the GNU GPL version 3 or later version of the license at your option.

Compile using the following command:

/* gcc factorial.c -o Factorial -lgmp */