/* This program is licensed under the GNU GPLv3 or later */
/* gcc factorial.c -o Factorial -lgmp */

#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main(int argc, char * argv[])
{
  mpz_t prime_number, number_to_factorize;
  int is_divisible;
  mpz_init_set_ui(prime_number,2); /* initialize prime_number */
  mpz_init_set_ui(number_to_factorize,20000001); /* initialize number_to_factorize */
    
  for (int i = 2; i <100000; i++)
  {
	  mpz_nextprime (prime_number , prime_number);
	  if (mpz_divisible_p(number_to_factorize,prime_number) != 0) /* returns non-zero if number_to_factorize is divisible by prime_number */
	  {
		    mpz_out_str(stdout,10,prime_number); /* output prime_number factor in base 10 */
            if (mpz_probab_prime_p(prime_number , 30) == 2) printf(" - Authentic prime!"); /* check if it is definately a prime or if it is a probabilistic prime */
            printf("\n");
	  }
  }
  
  mpz_clear(prime_number);  
  mpz_clear(number_to_factorize);

  return 0;
}


