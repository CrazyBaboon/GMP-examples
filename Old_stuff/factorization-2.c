/* This program is licensed under the GNU GPLv3 or later */
/* gcc factorial.c -o Factorial -lgmp */

#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main(int argc, char * argv[])
{
  mpz_t prime_number, number_to_factorize;
  int is_divisible;
  mpz_init_set_ui(prime_number,2); /* initialize prime_number */
  mpz_init_set_str (number_to_factorize, "135066410865995223349603216278805969938881475605667027524485143851526510604859533833940287150571909441798207282164471551373680419703964191743046496589274256239341020864383202110372958725762358509643110564073501508187510676594629205563685529475213500852879416377328533906109750544334999811150056977236890927563", 10);
  for (long int i = 10000000000; i <11000000000; i++)
  {
	  mpz_nextprime (prime_number , prime_number);
	  if (mpz_divisible_p(number_to_factorize,prime_number) != 0) /* returns non-zero if number_to_factorize is divisible by prime_number */
	  {
		    mpz_out_str(stdout,10,prime_number); /* output prime_number factor in base 10 */
            if (mpz_probab_prime_p(prime_number , 30) == 2) printf(" - Authentic prime!"); /* check if it is definately a prime or if it is a probabilistic prime */
            printf("\n");
	  }
  }
  
  mpz_clear(prime_number);  
  mpz_clear(number_to_factorize);

  return 0;
}
